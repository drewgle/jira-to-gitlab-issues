import * as request from "request-promise-native";
import * as fs from "fs";
import { v4 } from "uuid";

export class JiraExport {
    private JIRA_URL: string;
    private JIRA_PROJECT: string;
    private JIRA_ACCOUNT: IJiraAccount;

    public constructor(jiraUrl: string, jiraProject: string, jiraAccount: IJiraAccount) {
        this.JIRA_URL = jiraUrl;
        this.JIRA_PROJECT = jiraProject;
        this.JIRA_ACCOUNT = jiraAccount;
    }

    public async getIssueList(offset: number = 0, limit: number = 200): Promise<IJiraSearchResponse> {
        return await request({
            method: "get",
            url: `${this.JIRA_URL}/rest/api/2/search?jql=project=${this.JIRA_PROJECT}+order+by+id+asc&startAt=${offset}&maxResults=${limit}&fields=*all`,
            json: true,
            auth: {
                username: this.JIRA_ACCOUNT.username,
                password: this.JIRA_ACCOUNT.password
            }
        });
    }

    public async SaveAttachment(attachmentUrl: string): Promise<string> {
        const filename = v4();
        await request({
            method: "get",
            url: attachmentUrl,
            auth: {
                username: this.JIRA_ACCOUNT.username,
                password: this.JIRA_ACCOUNT.password
            }
        }).pipe(fs.createWriteStream(`uploads/${filename}`));
        return filename;
    }
}

export interface IJiraSearchResponse {
    total: number;
    startAt: number;
    maxResults: number;
    issues: IJiraIssueSearchResult[];
}

interface IJiraAccount {
    username: string;
    password: string;
}

export interface IJiraUser {
    name: string;
    emailAddress: string;
}

export interface IJiraIssueSearchResult {
    id: number;
    key: string;
    fields: IJiraIssueSearchFields;
}

interface IJiraIssueSearchFields {
    issuetype: {id: number; description: string; name: string; };
    project: {id: number; key: string; name: string; };
    fixVersions: IJiraVersion[];
    created: string;
    issuelinks: IJiraIssueLink[];
    updated: string;
    status: {name: string, id: number};
    description: string;
    summary: string;
    assignee: IJiraUser;
    creator: IJiraUser;
    reporter: IJiraUser;
    versions: IJiraVersion[];
    customfield_10045: number; //story points
    customfield_10056: string; //github url;
    customfield_10058: string; //Support Site Issue URL
    customfield_10059: IJiraUser; //engineer;
    customfield_10060: string; //Steps to Reproduce
    customfield_10062: string; //Build Number
    attachment: IJiraIssueAttachment[];
    comment: {comments: IJiraIssueComment[] };
}

export interface IJiraIssueLink {
    id: number;
    type: { name: string; inward: string; outward: string; };
    outwardIssue: { id: number; key: string; };
    inwardIssue: { id: number; key: string; };
}

interface IJiraVersion {
    id: number;
    description: string;
    name: string;
}

export interface IJiraIssueComment {
    id: number;
    author: IJiraUser;
    body: string;
    updateAuthor: IJiraUser;
    created: Date;
    updated: Date;
}
interface IJiraIssueAttachment {
    id: string;
    filename: string;
    author: IJiraUser;
    created: Date;
    Size: number;
    mimeType: string;
    content: string;
}
