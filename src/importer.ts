import { JiraExport, IJiraSearchResponse, IJiraIssueSearchResult, IJiraIssueComment, IJiraIssueLink } from "./jiraExport";
import { GitLabImport, IGitLabMergeRequest } from "./gitlabImport";
import mappings from "./mappings";

export class Importer {
    private jiraExport: JiraExport;
    private gitLabImport: GitLabImport;
    private jiraProject: string;
    private importedIssues: IImportedIssue[] = [];

    public constructor(jiraExport: JiraExport, gitLabImport: GitLabImport, jiraProject: string) {
        this.jiraExport = jiraExport;
        this.gitLabImport = gitLabImport;
        this.jiraProject = jiraProject;
    }

    public async Test(): Promise<void> {
        try {
          const mergeRequests = await this.gitLabImport.GetAllMergeRequests(this.jiraProject);
          const issueResponse = await this.jiraExport.getIssueList(74, 10);
          await this.importIssueList(mergeRequests, issueResponse.issues);
        } catch (e) {
          console.log(e);
        }
      }

    public async Import(): Promise<void> {
        const mergeRequests = await this.gitLabImport.GetAllMergeRequests(this.jiraProject);
        let issueResponse: IJiraSearchResponse = { startAt: 154, maxResults: 0, total: 0, issues: null};
        do {
          issueResponse = await this.jiraExport.getIssueList(issueResponse.startAt + issueResponse.maxResults);
          await this.importIssueList(mergeRequests, issueResponse.issues);
        } while (issueResponse.total > (issueResponse.startAt + issueResponse.maxResults));
        this.updateLinksWithGitLabIds();
        await this.addLinkComments();
    }

    private updateLinksWithGitLabIds(): void {
        this.importedIssues = this.importedIssues.map(ii => { return {
            gitLabId: ii.gitLabId,
            jiraId: ii.jiraId,
            linkedIssues: ii.linkedIssues.map(link => this.mapLinksToGitLabId(link)).filter(l => l !== null)
          }; });
    }

    private async addLinkComments(): Promise<void> {
        console.log("Adding link comments");
        for (const issue of this.importedIssues.filter(ii => ii.linkedIssues.length >= 1)) {
            await this.gitLabImport.AddIssueComment(mappings.mapIssueLinksToComment(issue.linkedIssues), issue.gitLabId);
        }
    }

    private mapLinksToGitLabId(link: IIssueLink): IIssueLink {
        const foundIssue = this.importedIssues.find((ii, index, array) => ii.jiraId === link.jiraIssue);
        if (typeof(foundIssue) === "undefined") {
            return null;
        }
        return {
                type: link.type,
                jiraIssue: link.jiraIssue,
                gitLabId: foundIssue.gitLabId
          };
    }

    private async importIssueList(mergeRequests: IGitLabMergeRequest[], issues: IJiraIssueSearchResult[]): Promise<void> {
      for (const issue in issues) {
        if (issues.hasOwnProperty(issue)) {
          const issueDetails = issues[issue];
          const githubUrl = issueDetails.fields.customfield_10056;
          let issueId: number;
          if (githubUrl !== null && typeof(githubUrl) !== "undefined" && githubUrl !== "") {
            issueId = await this.UpdateIssue(mergeRequests, issueDetails);
            console.log(`Update issue: ${issueId}`);
          } else {
            issueId = await this.importSingleIssue(mergeRequests, issueDetails);
            console.log(`jira: ${issueDetails.key}; gitlab#: ${issueId}`);
          }
          this.importedIssues.push({
              gitLabId: issueId,
              jiraId: issueDetails.key,
              linkedIssues: issueDetails.fields.issuelinks.map(this.mapIssueLink)
            });
        }
      }
    }

    private mapIssueLink(issueLink: IJiraIssueLink): IIssueLink {
        if (typeof(issueLink.outwardIssue) !== "undefined" && issueLink.outwardIssue != null) {
            return {type: issueLink.type.outward, jiraIssue: issueLink.outwardIssue.key};
        }
        return {type: issueLink.type.inward, jiraIssue: issueLink.inwardIssue.key};
    }

    private async UpdateIssue(mergeRequests: IGitLabMergeRequest[], issueDetails: IJiraIssueSearchResult): Promise<number> {
        const githubUrl = issueDetails.fields.customfield_10056;
        const githubIssueId = Number.parseInt(githubUrl.substr(githubUrl.lastIndexOf("/") + 1));
        await this.appendIssueDetails(issueDetails, mergeRequests, githubIssueId);
        return githubIssueId;
    }

    private async importSingleIssue(mergeRequests: IGitLabMergeRequest[], issueDetails: IJiraIssueSearchResult): Promise<number> {
        const gitlabIssueId = await this.gitLabImport.createIssue(mappings.mapJiraIssueToGitLab(issueDetails));
        await this.appendIssueDetails(issueDetails, mergeRequests, gitlabIssueId);
        return gitlabIssueId;
    }

    private async appendIssueDetails(issueDetails: IJiraIssueSearchResult, mergeRequests: IGitLabMergeRequest[], gitlabIssueId: number): Promise<void> {
        await this.gitLabImport.AddIssueComment(mappings.mapJiraAttributesToComment(issueDetails, mergeRequests), gitlabIssueId);
        await this.gitLabImport.UploadAttachments(await mappings.mapJiraAttachments(this.jiraExport, issueDetails), gitlabIssueId);
        await this.importComments(issueDetails.fields.comment.comments, gitlabIssueId);
        if (issueDetails.fields.status.name === "Done") {
            await this.gitLabImport.CloseIssue(gitlabIssueId);
        }
    }

    private async importComments(comments: IJiraIssueComment[], issueId: number): Promise<void> {
      for (const commentId in comments) {
        if (comments.hasOwnProperty(commentId)) {
          const comment = comments[commentId];
          await this.gitLabImport.AddIssueComment(mappings.mapJiraCommentToGitLabComment(comment), issueId);
        }
      }
    }
}

interface IImportedIssue {
    gitLabId: number;
    jiraId: string;
    linkedIssues: IIssueLink[];
}

interface IIssueLink {
    type: string;
    jiraIssue: string;
    gitLabId?: number;
}
