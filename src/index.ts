import { JiraExport } from "./jiraExport";
import { GitLabImport } from "./gitlabImport";
import { Importer } from "./importer";
import Credentials from "./credentials";

try {
  const jira = new JiraExport(Credentials.JIRA_URL, Credentials.JIRA_PROJECT, Credentials.JIRA_ACCOUNT);
  const gitLab = new GitLabImport(Credentials.GITLAB_URL, Credentials.GITLAB_TOKEN, Credentials.GITLAB_PROJECT);
  const importer = new Importer(jira, gitLab, Credentials.JIRA_PROJECT);
  importer.Import().catch(r => console.log(r));
} catch (e) {
    console.log(e);
}
