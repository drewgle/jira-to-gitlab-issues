import {JiraExport, IJiraUser, IJiraIssueSearchResult, IJiraIssueComment} from "./jiraExport";
import { IGitLabUser, IGitLabAttachment, IGitLabIssue, IGitLabComment, IGitLabMergeRequest } from "./gitlabImport";

export default {
    mapJiraUserToGitLab,
    mapJiraUserToGitLabId,
    mapJiraAttachments,
    mapJiraIssueToGitLab,
    mapJiraAttributesToComment,
    mapJiraCommentToGitLabComment,
    mapIssueLinksToComment
};

function mapJiraAttributesToComment(jiraIssue: IJiraIssueSearchResult, mergeRequests: IGitLabMergeRequest[]): IGitLabComment {
    return {
        body: `JIRA ISSUE #: ${jiraIssue.key}

Created by: @${mapJiraUserToGitLab(jiraIssue.fields.creator).name}

Created at: ${jiraIssue.fields.created}

Last Updated at: ${jiraIssue.fields.updated}

Reporter: @${mapJiraUserToGitLab(jiraIssue.fields.reporter).name}

Assignee: @${mapJiraUserToGitLab(jiraIssue.fields.assignee).name}

Engineer: @${mapJiraUserToGitLab(jiraIssue.fields.customfield_10059).name}

Issue type: ${jiraIssue.fields.issuetype.name}

Project: ${jiraIssue.fields.project.name}

Fix Version: ${jiraIssue.fields.fixVersions.map(f => f.name).join(", ")}

Affected Version: ${jiraIssue.fields.versions.map(f => f.name).join(", ")}

SupportSiteUrl: ${jiraIssue.fields.customfield_10058 || ""}

Weight: ${jiraIssue.fields.customfield_10045 || "0"}

Build Number: ${jiraIssue.fields.customfield_10062 || ""}

Related Merge Requests: ${mergeRequests.filter(m => m.title.includes(jiraIssue.key) || m.description.includes(jiraIssue.key)).map(m => `!${m.iid}`).join(", ")}

Steps to Reproduce: ${jiraIssue.fields.customfield_10060 || ""}`
    };
}

function mapJiraIssueToGitLab(JIRAIssue: IJiraIssueSearchResult): IGitLabIssue {
  return {
    title: JIRAIssue.fields.summary,
    description: JIRAIssue.fields.description,
    labels: "",
    created_at: JIRAIssue.fields.created,
    assignee_ids: [
        mapJiraUserToGitLabId(JIRAIssue.fields.reporter),
        mapJiraUserToGitLabId(JIRAIssue.fields.assignee),
        mapJiraUserToGitLabId(JIRAIssue.fields.customfield_10059)].filter( a => a !== null && typeof(a) !== "undefined")
  };
}

function mapJiraCommentToGitLabComment(comment: IJiraIssueComment): IGitLabComment {
    return {
        body: `Original Author: @${mapJiraUserToGitLab(comment.author).name}

Created at: ${comment.created}

Last updated by: @${mapJiraUserToGitLab(comment.updateAuthor).name}

Last updated at: ${comment.updated}

======

${comment.body}`
    };
}

function mapJiraUserToGitLab(jiraUser: IJiraUser): IGitLabUser {
    const defaultUser = "";
    const mapping: {[id: string]: IGitLabUser } = {
        "": {name: "", id: null},
        "admin": {name: "lisacockrell", id: 2142079},
        "drew.carpenter": {name: "drewgle", id: 2142085},
        "brian.painter": {name: "Brian-Painter", id: 2142075},
        "dan.luhring": {name: "danluhring", id: 1948055},
        "dino.hall": {name: "dino.hall", id: 2142145},
        "lisa.cockrell": {name: "lisacockrell", id: 2142079},
        "derek.pace": {name: "derekpace", id: 2142123},
        "ed.okolovitch": {name: "edokolovitch", id: 2142114},
        "jordan.upperman": {name: "Jordan.Upperman", id: 2142116},
        "lee.perkins": {name: "lee.perkins", id: 2142082},
        "mike.brinn": {name: "mike.brinn", id: 2142097},
        "tiffany.abreu": {name: "tiffanyabreu", id: 2142117},
        "dan.flowers": {name: "dan.flowers", id: 2142268},
        "joe.gabana": {name: "joe.gabana", id: 2143374},
        "noel.lucas": {name: "noel.lucas", id: 2143434},
        "travis.cupp": {name: "tra-vis-cupp", id: 2143374},
    };
    try {
        return mapping[jiraUser.name] || mapping[defaultUser];
    } catch (error) {
        return mapping[defaultUser];
    }
}

function mapIssueLinksToComment(links: IIssueLink[]): IGitLabComment {
    return {
        body: `Linked Issues:

${links.map(issueLinkToString).join("\r\n\r\n")}`
    };
}

function issueLinkToString(link: IIssueLink): string {
    return `${link.type} #${link.gitLabId || link.jiraIssue}`;
}

async function mapJiraAttachments(exporter: JiraExport, JIRAIssue: IJiraIssueSearchResult): Promise<IGitLabAttachment[]> {
    const gitlabAttachments: IGitLabAttachment[] = [];
    for (const attachment of JIRAIssue.fields.attachment) {
        gitlabAttachments.push( {
            attachmentPath: await exporter.SaveAttachment(attachment.content),
            filename: attachment.filename
        });
    }
    return gitlabAttachments;
}

function mapJiraUserToGitLabId(jiraUser: IJiraUser): number {
    try {
        return mapJiraUserToGitLab(jiraUser).id;
    } catch (error) {
        return null;
    }
}

interface IIssueLink {
    type: string;
    jiraIssue: string;
    gitLabId?: number;
}
